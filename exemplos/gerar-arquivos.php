<?php

require 'vendor/autoload.php';

$contexto = ['usuario' => 'Meu Usuário', 'php_versao' => PHP_VERSION];

$meuLog = \Singleton\MeuLog::getInstance();
$meuLog->info('PRIMEIRO LOG', $contexto);

$meuLog2 = \Singleton\MeuLog::getInstance();
$meuLog2->info('SEGUNDO LOG', $contexto);

if ($meuLog === $meuLog2) {
    echo 'As duas varáveis possuem o mesmo objeto.';
} else {
    echo 'As variáveis possuem objetos diferentes.';
}

echo PHP_EOL;