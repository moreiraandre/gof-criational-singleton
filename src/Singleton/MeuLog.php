<?php

namespace Singleton;

use Psr\Log\AbstractLogger;

class MeuLog extends AbstractLogger
{
    private static MeuLog $instance;

    private function __construct()
    {
    }

    protected function __clone()
    {
    }

    public function __wakeup()
    {
        throw new \Exception("Cannot unserialize a singleton.");
    }


    public static function getInstance(): MeuLog
    {
        if (!isset(self::$instance)) {
            self::$instance = new static();
        }

        return self::$instance;
    }

    public function log($level, \Stringable|string $message, array $context = []): void
    {
        $log = '['
            . date('Y-m-d H:i:s')
            . '] '
            . "$level: $message "
            . PHP_EOL
            . print_r($context, true)
            . PHP_EOL;

        $nomeArquivo = './exemplos/tmp/' . date('Y_m_d') . '.log';
        file_put_contents($nomeArquivo, $log, FILE_APPEND);

        echo "Log adicionado no arquivo $nomeArquivo" . PHP_EOL;
    }
}