<?php

namespace Tests\Feature;

use PHPUnit\Framework\TestCase;

class SingletonTest extends TestCase
{
    public function testSucesso()
    {
        $contexto = ['usuario' => 'Meu Usuário', 'php_versao' => PHP_VERSION];

        $meuLog = \Singleton\MeuLog::getInstance();
        $meuLog->info('PRIMEIRO LOG', $contexto);

        $meuLog2 = \Singleton\MeuLog::getInstance();
        $meuLog2->info('SEGUNDO LOG', $contexto);

        $this->assertTrue($meuLog === $meuLog2);
    }
}